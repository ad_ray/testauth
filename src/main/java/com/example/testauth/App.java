/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.testauth;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author adray
 */
@ApplicationPath("test")
public class App extends ResourceConfig{
    public App() {
        super();
        packages("com.example.testauth");
    }
}
