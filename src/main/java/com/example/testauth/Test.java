/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.testauth;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author adray
 */
@Path("test")
public class Test {
    
    @Context
    SecurityContext sc;
    
    @Context
    private HttpHeaders headers;
    
    @GET
    @Path("user")
    public String test() {
        return sc.getUserPrincipal() != null 
                ? sc.getUserPrincipal().getName() : "null";
    }
    
    @GET
    @Path("headers")
    @Produces({MediaType.APPLICATION_JSON})
    public MultivaluedMap<String, String>  headers() {
       return headers.getRequestHeaders();
    }
    
    @GET
    @Path("body")
    public String test1(@Context HttpServletRequest request) throws IOException {
        return org.apache.commons.io.IOUtils.toString(request.getInputStream());
    
    }
    
}
